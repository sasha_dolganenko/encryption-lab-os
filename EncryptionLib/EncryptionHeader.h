#pragma once
#ifndef _ENCRYPTION_LIB
#define _ENCRYPTION_LIB
#ifdef RSA_EXPORTS
#define PREFIX extern "C" _declspec (dllexport)
#else
#define PREFIX extern "C" _declspec (dllimport)
#endif

//#define PREFIX __declspec(dllimport)

PREFIX void __stdcall Encrypt(char * message, int length, unsigned  int * encryptedOutput, int* decryptKey, int* n);

PREFIX void __stdcall Decrypt(unsigned int * encryptedInput, int length, char * decryptedOutput, int decryptKey, int n);

//unsigned int powMod(unsigned x, unsigned y, unsigned m);

void encryptAndDecryptKeys(unsigned int *encryptionKey,  int * decryptionKey, int* n);

//unsigned int primeNumber();
#endif