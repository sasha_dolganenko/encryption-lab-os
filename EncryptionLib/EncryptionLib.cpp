// EncryptionLib.cpp: определяет экспортированные функции для приложения DLL.
//

#include "stdafx.h"
#include <stdio.h>
#include "EncryptionHeader.h"
#include "EncryptionMath.h"


 void __stdcall Encrypt(char * message, int length, unsigned int * encryptedOutput, int* decryptKey, int* n) {
	 unsigned int encryptKey;
	 encryptAndDecryptKeys(&encryptKey, decryptKey, n);
	 printf("Encrypt key: %d\n", encryptKey);
	 printf("Decrypt key: %d\n", &decryptKey);
	 for (int i = 0; i < length; i++)
	 {
		 unsigned int encodedChar = PowMod(message[i], encryptKey, *n); 
		//printf("Before: %d      after: %lu  %c\n",message[i],encodedChar, (char)encodedChar);
		encryptedOutput[i] = encodedChar;
	 }
}

 void __stdcall Decrypt(unsigned int * encryptedInput, int length, char * decryptedOutput, int decryptKey, int n) {
	 for (int i = 0; i < length; i++)
	 {
		 unsigned int decodedChar = PowMod(encryptedInput[i], decryptKey, n);
		 decryptedOutput[i] = decodedChar;
		// printf("After decode: %d      in char: %c\n", decodedChar, (char)decodedChar);
	 }
 }
 

 void encryptAndDecryptKeys(unsigned int *encryptionKey,  int *decryptionKey, int* n) {
	 int first;
	 int second;
	 do {
		 first = Prime(); 
		 second = Prime();
	 } while (first == second);
	 *n = first * second;

	 unsigned int fi = (first - 1) * (second - 1);
	 for (*encryptionKey = 3; GreateCommonDivider(*encryptionKey, fi) != 1; *encryptionKey += 2);
	 for (*decryptionKey = 3; (*encryptionKey) * (*decryptionKey) % fi != 1; *decryptionKey += 1);
 }