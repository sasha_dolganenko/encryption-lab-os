#include "stdafx.h"
#include <stdlib.h>
#include "EncryptionMath.h"


unsigned Prime (){
	unsigned x = rand() % 100; x |= 1;
	while (1) {
		unsigned p;
		for (unsigned i = 3; ;i += 2) {
			p = i * i;
			if (p > x) break;
			if (x % i == 0) break;
		}
		if (p >x) return x;
		x += 2;
	}	 
}

unsigned GreateCommonDivider(unsigned x, unsigned y) {
	while (x && y) {
		if (x > y)x %= y;
		else y %= x;
	}
	return x + y;
}

/*
unsigned int PowMod2(unsigned x, unsigned y, unsigned m) {
	unsigned r = 1;
	for (unsigned i = 0; i < y; i++) {
		r *= x;
		r %= m;
	}
	return r;
}
*/

unsigned int PowMod(unsigned base, unsigned power, unsigned modulo) {
	if (power == 0) {
		return 1;
	}
	unsigned int mask = 0x80000000;
	unsigned int result = base;
	while ((mask & power) != mask) {
		mask >>= 1;
	}
	while (1) {
		mask >>= 1;
		if (mask == 0) {
			break;
		}
		result *= result;
		if ((power & mask) != 0) {
			result *= base;
		}
		result %= modulo;
	}
	return result;
}