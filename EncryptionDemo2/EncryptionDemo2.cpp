// EncryptionDemo2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "windows.h"
#include <locale.h>

typedef void (*ENCRYPTFUNC)(char * message, int length, unsigned int * encryptedOutput, int* decryptKey, int* n);

typedef void (*DECRYPTFUNC)(unsigned int * encryptedInput, int length, char * decryptedOutput, int decryptKey, int n);


void printEncryptedMessage(unsigned int* message, int length) {
	printf("Printing encrypted message: \n");
	for (int i = 0; i < length; i++)
	{
		printf("%c", message[i]);
	}
	printf("\n\n");
}

void printDecryptedMessage(char* message, int length) {
	printf("Printing decrypted message: \n");
	for (int i = 0; i < length; i++)
	{
		printf("%c", message[i]);
	}
	printf("\n\n");
}

void printResource(TCHAR* message, int length) {
	printf("Printing resource: \n");
	_tprintf(_T("%s\n"), message);
	/*
	for (int i = 0; i < length; i++)
	{
		_tprintf(_T("%c"), message[i]);
	}
	printf("\n\n");
	*/
}



int main()
{
	system("chcp 1251");
	_tsetlocale(LC_ALL, _T("Russian"));
	//SetConsoleCP(1251);
	//SetConsoleOutputCP(1251);
	HINSTANCE encryptionDLL = NULL;
	HMODULE localizationDLL = NULL;
	ENCRYPTFUNC encryptFunc;
	DECRYPTFUNC decryptFunc;
	TCHAR encryptionLibname[] = _T("EncryptionLib.dll");
	encryptionDLL = LoadLibrary(encryptionLibname);

	//HRSRC resource = NULL;
	localizationDLL = LoadLibrary(_T("RussianLanguage"));

	printf("Is encryption dll null? %s\n", encryptionDLL == NULL ? "true" : "false");
	printf("Is localization dll null? %s\n", localizationDLL == NULL ? "true" : "false");
	//resource = FindResource(localizationDLL, MAKEINTRESOURCE(101), _T("RT_STRING"));
	_tsetlocale(LC_ALL, _T("Russian"));
	setlocale(LC_ALL, "Russian");
	TCHAR resourceValue[15];
	LoadString(localizationDLL, 101, resourceValue, 15);
	
	printf("Is the resource null? %s\n", resourceValue == NULL ? "true" : "false");
	printResource(resourceValue, 100);
	if (encryptionDLL != NULL) {
		encryptFunc =(ENCRYPTFUNC) GetProcAddress(encryptionDLL, "Encrypt");
		printf("Is the encrypt function  null? %s\n", encryptFunc == NULL ? "true" : "false");
		decryptFunc = (DECRYPTFUNC)GetProcAddress(encryptionDLL, "Decrypt");
		printf("Is the decrypt function  null? %s\n", encryptFunc == NULL ? "true" : "false");
		char message[] = "ABCDEFG hijklmnop qrs tuv   wxynz";
		int size = strlen(message);
		unsigned int*  encryptedOutput = (unsigned int *)malloc(size * sizeof(unsigned int));
		int key;
		int n;
		printDecryptedMessage(message, size);
		encryptFunc(message, size, encryptedOutput, &key, &n);
		printEncryptedMessage(encryptedOutput, size);

		printf("\nDecrypting this message back\n");
		char* decryptedMessage = (char*)malloc(size * sizeof(char));
		decryptFunc(encryptedOutput, size, decryptedMessage, key, n);
		printDecryptedMessage(decryptedMessage, size);
		free(encryptedOutput);
		free(decryptedMessage);

		
	}
    return 0;

}

