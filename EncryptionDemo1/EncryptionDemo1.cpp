// EncryptionDemo1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "EncryptionHeader.h"
#include <stdio.h>
#include <stdlib.h>
#include "DemoHeader.h"
#include <string.h>

int main()
{
	char message[] = "abcdefg hijklmnop qrs tuv   wxynz";
	int size =  strlen(message);
	unsigned int*  encryptedOutput = (unsigned int *) malloc(size * sizeof(unsigned int));
	int key;
	int n;
	printDecryptedMessage(message, size);
	Encrypt(message, size,encryptedOutput, &key, &n);
	printEncryptedMessage(encryptedOutput, size);

	printf("\nDecrypting this message back\n");
	char* decryptedMessage = (char*)malloc(size * sizeof(char));
	Decrypt(encryptedOutput, size, decryptedMessage, key, n);
	printDecryptedMessage(decryptedMessage, size);
	free(encryptedOutput);
	free(decryptedMessage);
    return 0;
}



void printEncryptedMessage(unsigned int* message, int length) {
	printf("Printing encrypted message: \n");
	for (int i = 0; i < length; i++)
	{
		printf("%c", message[i]);
	}
	printf("\n\n");
}

void printDecryptedMessage(char* message, int length) {
	printf("Printing decrypted message: \n");
	for (int i = 0; i < length; i++)
	{
		printf("%c", message[i]);
	}
	printf("\n\n");
}
